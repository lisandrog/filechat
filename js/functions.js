$(document).ready(function(){

	$("#logout_link").click(function(){
		var confirmation = confirm("Do you want to end session?");
		if(confirmation==true){
            window.location = 'index.php?logout=true';
        }
	});

    //If user submits the form
	$("#send_button").click(function(){	
		var user_message = $("#user_message").val();
        $.post("./php/post.php", 
            {
                text: user_message
            });				
        $("#user_message").attr("value", "");
        
		return false;
    });
    
    $("#empty_chat").click(function(){	
        $.post("./php/post.php", 
            {
                empty_chat: true
            });				
        $("#user_message").attr("value", "");
        
		return false;
    });
    

    //Load the file containing the chat log
    function loadLog(){		
        $.ajax({
            url: "log.html",
            cache: false,
            success: function(html){		
                $("#chatbox").html(html); //Insert chat log into the #chatbox div				

                //Auto-scroll
                try{
                    var newscrollHeight = $("#chatbox").attr("scrollHeight") - 20; //Scroll height after the request
                    if(newscrollHeight > oldscrollHeight){
                        $("#chatbox").animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
                    }	
                }catch(ignored){}
            },
        });
    }
    setInterval (loadLog, 2500);	//Reload file every 2500 ms or x ms if you wish to change the second parameter
    
});
