<?
    session_start();

    include_once('./php/functions.php');

    if(isset($_GET['logout'])){ 
        logOut();
    }

    if(isset($_POST['enter'])){
        logIn();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lite Chat From Scratch No Db</title>
        <link type="text/css" rel="stylesheet" href="./css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <?php
        if(!isset($_SESSION['name'])){
            loginForm();
        }else{
    ?>
    <div id="wrapper">
        <div id="menu">
            <p class="user_slug">User: <b><?php echo $_SESSION['name']; ?></b></p>
            <p class="logout"><a id="logout_link" href="#">Logout</a></p>
            <div style="clear:both"></div>
        </div>    

        <div id="chatbox">
            <?php
                if(file_exists("log.html") && filesize("log.html") > 0){
                    $handle = fopen("log.html", "r");
                    $contents = fread($handle, filesize("log.html"));
                    fclose($handle);
                    
                    echo $contents;
                }
            ?>
        </div>
        
        <form name="message" action="">
            <input name="user_message" type="text" id="user_message" size="60" />
            <input name="send_button" type="submit"  id="send_button" value="send" />
            <input name="empty_chat" type="submit"  id="empty_chat" value="empty" />
        </form>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    <script type="text/javascript" src="./js/functions.js"></script>
    <?php
        }
    ?>
    </body>
</html>