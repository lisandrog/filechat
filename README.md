# Small Chat App

Small chat app, from scratch, no db.


## Instalation 
Just have to download the project, put into a php server (apache), and thats all.

## Usage 
This is not a pro app, this have no security, and can be improve of many ways. 
I just make it for fun, and to share the code. Maybe helps someone.

## Contributing 
If you want to contribute just make a new branch and share your features. 

The intention of this app if make it the simplest possible.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Supporting
[You can buy me a coffee if you want!](https://www.buymeacoffee.com/lisandrog)