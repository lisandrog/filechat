<?
    session_start();
    if(isset($_SESSION['name'])){

        if(isset($_POST) && isset($_POST['text'])){
            $text = $_POST['text'];
            if(!empty($text) && $text !== ""){    
                $fp = fopen("../log.html", 'a');

                $chat_line = "<div class='message_line'>(".date("g:i A").") <b>".$_SESSION['name']."</b>: ".
                stripslashes(htmlspecialchars($text))."<br></div>\n";

                fwrite($fp, $chat_line);

                fclose($fp);
            }
        }

        if(isset($_POST) && isset($_POST['empty_chat'])){
            $log_content = file_get_contents("../log.html");

            file_put_contents("../history.html", $log_content, FILE_APPEND | LOCK_EX);

            $chat_line =  "<div class='message_line'>(".date("g:i A").") <b>".$_SESSION['name'].
            "</b>: has empty the chat box<br></div>\n";

            file_put_contents("../log.html",$chat_line, LOCK_EX);
        }

    }
?>